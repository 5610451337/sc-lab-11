package hashMap;

import java.util.HashMap;

public class WordCounter {
	private String message;
	HashMap<String,Integer> wordCount = new HashMap();
	
	public WordCounter(String amessage){
		message = amessage;
	}
	public void count() {
		String[] part = message.split(" ");
		for(String a : part){
			if(wordCount.containsKey(a)){
				wordCount.put(a, wordCount.get(a)+1);
			}
			else{
				wordCount.put(a, 1);
			}
		}
	}
	
	
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			int a = wordCount.get(word);
			return a;
		}
		return 0;
	}

}
