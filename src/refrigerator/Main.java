package refrigerator;

public class Main {
	public static void main(String[] args){
		Refrigerator refri = new Refrigerator();


		try{
			refri.put("apple");
			System.out.println(refri.takeOut("apple"));
			System.out.println(refri.takeOut("Milk"));
			refri.put("Water");
			refri.put("Pork");
			System.out.println(refri.toString());
			refri.put("Egg");
		}catch(FullException e){
			System.err.println("Error: " + e.getMessage());
		}
		
	}
}