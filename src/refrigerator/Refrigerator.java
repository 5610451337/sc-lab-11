package refrigerator;

import java.util.ArrayList;

public class Refrigerator {
	private int size = 2;
	private ArrayList<String> things = new ArrayList();
	
	public void put(String stuff) throws FullException{
		if(things.size()==size){
			throw new FullException("Refrigerator is Full ");
		}
		things.add(stuff);
	}
	public String takeOut(String stuff){
		if(things.contains(stuff)){
			things.remove(stuff);
			return stuff;
		}
		return null;
	}
	public String toString(){
		String str="";
		for(String a : things){
			str+= a +" ";
		}
		return str;
	}
	
}
